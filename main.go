package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/alecthomas/template"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"github.com/swaggo/swag"
	"strconv"
)

const JsonByteStreamHeader = "application/json; charset=utf-8"

var db *sql.DB

// @title Doctor service
// @version 1.0
// @description Service to create, modify and search doctors

// @contact.name API Support
// @contact.email z5061594@student.unsw.edu.au

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @BasePath /

func main() {
	//Set up our router
	var err error
	db, err = OpenDBConnection("root", "12345678", "doctorService", "assignment-cluster.cluster-c0wudp930tjr.ap-southeast-2.rds.amazonaws.com")
	if err != nil {
		fmt.Println(err)
	}

	r := gin.Default()
	r.POST("/doctor", PostDoctorAPI)
	r.GET("/doctor", GetDoctorsAPI)
	r.GET("/doctor/:doctorID", GetDoctorAPI)
	r.PUT("/doctor/:doctorID", PutDoctorAPI)
	r.DELETE("/doctor/:doctorID", DeleteDoctorAPI)

	config := &ginSwagger.Config{
		URL: "http://localhost:5000/swagger/doc.json", //The url pointing to API definition
	}
	// use ginSwagger middleware to
	r.GET("/swagger/*any", ginSwagger.CustomWrapHandler(config, swaggerFiles.Handler))

	r.Run(":5000") // listen and serve
}

// PostDoctorAPI godoc
// @Summary Create a Doctor
// @Description Create a doctor by providing the nessasary doctor information
// @Tags Doctor
// @Accept  json
// @Produce  json
// @Param BookingInfo body DoctorInfo true "Booking information"
// @Success 200 {object} Doctor
// @Failure 400 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /doctor [post]
func PostDoctorAPI(c *gin.Context) {
	decoded := new(DoctorInfo)
	jsonDecodeErr := json.NewDecoder(c.Request.Body).Decode(&decoded)

	if jsonDecodeErr != nil {
		ReturnFailure(c, 400, jsonDecodeErr)
		return
	}

	createdDoctor, err := CreateDoctor(*decoded)

	if err != nil {
		ReturnFailure(c, 500, err)
	} else {
		SendResponse(c, createdDoctor, 200)
	}
}

type GetDoctorsResponse struct {
	Doctors []*Doctor
}

// GetDoctorsAPI godoc
// @Summary Get doctors
// @Description Get a list of doctors, filterable by name
// @Tags Doctor
// @Accept  json
// @Produce  json
// @Param name query string false "Filter by doctors with the given name"
// @Success 200 {object} Doctor
// @Failure 400 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /doctor [get]
func GetDoctorsAPI(c *gin.Context) {
	var toReturn []*Doctor
	var err error
	name, namePresent := c.GetQuery("name")

	if namePresent {
		toReturn, err = GetDoctorsByName(name)
	} else {
		toReturn, err = GetDoctors()
	}

	if err != nil {
		ReturnFailure(c, 500, err)
	} else {
		resp := new(GetDoctorsResponse)
		resp.Doctors = toReturn

		SendResponse(c, resp, 200)
	}
}

type DeleteDoctorsResponse struct {
	DeletedID int64
}

// DeleteDoctorAPI godoc
// @Summary Delete a doctor
// @Description Deletes a doctor
// @Tags Doctor
// @Accept  json
// @Produce  json
// @Param ID path string true "Doctor ID to be deleted"
// @Success 200 {object} DeleteDoctorsResponse
// @Failure 400 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /doctor/{id} [delete]
func DeleteDoctorAPI(c *gin.Context) {
	doctor, err := validateGivenParams(c)

	if err != nil {
		return
	}

	deleteErr := DeleteDoctor(doctor.ID)

	if deleteErr != nil {
		ReturnFailure(c, 500, deleteErr)
		return
	}

	toReturn := new(DeleteDoctorsResponse)
	toReturn.DeletedID = doctor.ID

	SendResponse(c, toReturn, 200)
}

// GetDoctorAPI godoc
// @Summary Get a doctor
// @Description Gets a doctor that matches the given ID
// @Tags Doctor
// @Accept  json
// @Produce  json
// @Param ID path string true "Doctor ID"
// @Success 200 {object} Doctor
// @Failure 400 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /doctor/{id} [get]
func GetDoctorAPI(c *gin.Context) {
	doctor, err := validateGivenParams(c)

	if err == nil {
		SendResponse(c, doctor, 200)
	}
}

// PutDoctorAPI godoc
// @Summary Modify a doctor
// @Description Modifies/updates the doctor record at the given ID
// @Tags Doctor
// @Accept  json
// @Produce  json
// @Param ID path string true "Doctor ID"
// @Success 200 {object} Doctor
// @Failure 400 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /doctor/{id} [put]
func PutDoctorAPI(c *gin.Context) {
	doctor, err := validateGivenParams(c)

	if err != nil {
		return
	}

	decoded := new(DoctorInfo)
	jsonDecodeErr := json.NewDecoder(c.Request.Body).Decode(&decoded)

	if jsonDecodeErr != nil {
		ReturnFailure(c, 400, jsonDecodeErr)
		return
	}

	newDoctor, modErr := ModifyDoctor(*doctor, *decoded)

	if modErr != nil {
		ReturnFailure(c, 500, modErr)
		return
	}

	SendResponse(c, newDoctor, 200)
}

func validateGivenParams(c *gin.Context) (*Doctor, error) {
	stringID := c.Param("doctorID")

	if stringID == "" {
		err := errors.New("Missing doctor ID in path paramaters")
		ReturnFailure(c, 400, err)
		return nil, err
	}

	parsedID, parseErr := strconv.ParseInt(stringID, 10, 64)

	if parseErr != nil {
		err := errors.New("Invalid Doctor ID given")
		ReturnFailure(c, 400, err)
		return nil, err
	}

	doctor, err := GetDoctor(parsedID)

	if err != nil {
		ReturnFailure(c, 500, err)
		return nil, err
	}

	if doctor == nil {
		err := errors.New("Doctor not found")
		ReturnFailure(c, 404, err)
		return nil, err
	}

	return doctor, nil
}

var doc = `{
  "basePath": "/",
  "info": {
    "contact": {
      "email": "z5061594@student.unsw.edu.au",
      "name": "API Support"
    },
    "description": "Service to create, modify and search doctors",
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    },
    "title": "Doctor service",
    "version": "1.0"
  },
  "paths": {
    "/doctor": {
      "get": {
        "consumes": [
          "application/json"
        ],
        "description": "Get a list of doctors, filterable by name",
        "parameters": [
          {
            "description": "Filter by doctors with the given name",
            "in": "query",
            "name": "name",
            "type": "string"
          }
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Doctor"
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          }
        },
        "summary": "Get doctors",
        "tags": [
          "Doctor"
        ]
      },
      "post": {
        "consumes": [
          "application/json"
        ],
        "description": "Create a doctor by providing the nessasary doctor information",
        "parameters": [
          {
            "description": "Booking information",
            "in": "body",
            "name": "BookingInfo",
            "required": true,
            "schema": {
              "$ref": "#/definitions/DoctorInfo",
              "type": "object"
            }
          }
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/Doctor",
              "type": "object"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          }
        },
        "summary": "Create a Doctor",
        "tags": [
          "Doctor"
        ]
      }
    },
    "/doctor/{id}": {
      "delete": {
        "consumes": [
          "application/json"
        ],
        "description": "Deletes a doctor",
        "parameters": [
          {
            "description": "Doctor ID to be deleted",
            "in": "path",
            "name": "id",
            "required": true,
            "type": "string"
          }
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/DeleteDoctorResponse",
              "type": "object"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          }
        },
        "summary": "Delete a doctor",
        "tags": [
          "Doctor"
        ]
      },
      "get": {
        "consumes": [
          "application/json"
        ],
        "description": "Gets a doctor that matches the given ID",
        "parameters": [
          {
            "description": "Doctor ID",
            "in": "path",
            "name": "id",
            "required": true,
            "type": "string"
          }
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/Doctor",
              "type": "object"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          }
        },
        "summary": "Get a doctor",
        "tags": [
          "Doctor"
        ]
      },
      "put": {
        "consumes": [
          "application/json"
        ],
        "description": "Modifies/updates the doctor record at the given ID",
        "parameters": [
          {
            "description": "Doctor ID",
            "in": "path",
            "name": "id",
            "required": true,
            "type": "string"
          }
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/Doctor",
              "type": "object"
            }
          },
          "400": {
            "description": "Bad Request",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          },
          "500": {
            "description": "Internal Server Error",
            "schema": {
              "$ref": "#/definitions/ErrorResponse",
              "type": "object"
            }
          }
        },
        "summary": "Modify a doctor",
        "tags": [
          "Doctor"
        ]
      }
    }
  },
  "swagger": "2.0",
  "definitions": {
    "ErrorResponse": {
      "type": "object",
      "properties": {
        "Error": {
          "type": "string"
        }
      }
    },
    "DoctorInfo": {
      "type": "object",
      "properties": {
        "FullName": {
          "type": "string"
        },
        "Location": {
          "type": "string"
        },
        "Specialisation": {
          "type": "string"
        }
      }
    },
    "DoctorMeta": {
      "type": "object",
      "properties": {
        "ID": {
          "type": "integer",
          "format": "int64"
        },
        "CreatedAt": {
          "type": "integer",
          "format": "int64"
        }
      }
    },
    "Doctor": {
      "allOf": [
        {
          "$ref": "#/definitions/DoctorInfo"
        },
        {
          "$ref": "#/definitions/DoctorMeta"
        }
      ]
    },
    "DeleteDoctorResponse": {
      "type": "object",
      "properties": {
        "DeletedID": {
          "type": "integer",
          "format": "int64"
        }
      }
    }
  }
}`

type swaggerInfo struct {
	Version     string
	Host        string
	BasePath    string
	Title       string
	Description string
}

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo swaggerInfo

type s struct{}

func (s *s) ReadDoc() string {
	t, err := template.New("swagger_info").Parse(doc)
	if err != nil {
		return doc
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, SwaggerInfo); err != nil {
		return doc
	}

	return tpl.String()
}

func init() {
	swag.Register(swag.Name, &s{})
}
