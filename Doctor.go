package main

import (
	"errors"
	"time"
)

type Doctor struct {
	DoctorMeta
	DoctorInfo
}

type DoctorMeta struct {
	ID        int64
	CreatedAt int64
}

type DoctorInfo struct {
	FullName       string
	Location       string
	Specialisation string
}

func (g *Doctor) IsNull() bool {
	return g.ID == 0
}

func CreateDoctor(info DoctorInfo) (*Doctor, error) {
	d := new(Doctor)
	currTime := time.Now().Unix()
	exec, initialExecErr := db.Exec("INSERT INTO Doctor (FullName, Location, Specialisation, CreatedAt) VALUES (?,?,?,?)", info.FullName, info.Location, info.Specialisation, currTime)

	if initialExecErr != nil {
		return nil, initialExecErr
	}

	id, idErr := exec.LastInsertId()

	if idErr != nil {
		return nil, idErr // this needs to be fully fleshed
	}

	d.ID = id
	d.CreatedAt = currTime
	d.FullName = info.FullName
	d.Location = info.Location
	d.Specialisation = info.Specialisation

	return d, nil
}

func ModifyDoctor(existingDoctor Doctor, newInfo DoctorInfo) (*Doctor, error) {
	if existingDoctor.IsNull() {
		return nil, errors.New("Cannot modify a nil doctor")
	}

	toInsertDoctor := new(Doctor)

	if newInfo.FullName != "" {
		toInsertDoctor.FullName = newInfo.FullName
	} else {
		toInsertDoctor.FullName = existingDoctor.FullName
	}

	if newInfo.Location != "" {
		toInsertDoctor.Location = newInfo.Location
	} else {
		toInsertDoctor.Location = existingDoctor.Location
	}

	if newInfo.Specialisation != "" {
		toInsertDoctor.Specialisation = newInfo.Specialisation
	} else {
		toInsertDoctor.Specialisation = existingDoctor.Specialisation
	}

	toInsertDoctor.DoctorMeta = existingDoctor.DoctorMeta

	_, execErr := db.Exec("UPDATE Doctor SET FullName=?, Location=?, Specialisation=? WHERE ID=?", toInsertDoctor.FullName, toInsertDoctor.Location, toInsertDoctor.Specialisation, existingDoctor.ID)

	if execErr != nil {
		return nil, execErr
	}

	return toInsertDoctor, nil
}

func GetDoctors() ([]*Doctor, error) {
	var toReturn []*Doctor

	values, dbErr := db.Query("SELECT ID, FullName, Location, Specialisation, CreatedAt from Doctor")

	if dbErr != nil {
		return toReturn, dbErr
	}

	for values.Next() {
		newObject := new(Doctor)

		values.Scan(&newObject.ID, &newObject.FullName, &newObject.Location, &newObject.Specialisation, &newObject.CreatedAt)

		toReturn = append(toReturn, newObject)
	}

	return toReturn, nil
}

func GetDoctor(ID int64) (*Doctor, error) {
	toReturn := new(Doctor)

	values, dbErr := db.Query("SELECT ID, FullName, Location, Specialisation, CreatedAt from Doctor where ID=?", ID)
	values.Next()

	if dbErr != nil {
		return toReturn, dbErr
	}
	values.Scan(&toReturn.ID, &toReturn.FullName, &toReturn.Location, &toReturn.Specialisation, &toReturn.CreatedAt)

	if toReturn.IsNull() {
		return nil, nil
	}

	return toReturn, nil
}

func DeleteDoctor(ID int64) error {
	_, execErr := db.Exec("DELETE FROM Doctor where ID=?", ID)

	return execErr
}

func GetDoctorsByName(name string) ([]*Doctor, error) {
	var toReturn []*Doctor

	values, dbErr := db.Query("SELECT ID, FullName, Location, Specialisation, CreatedAt from Doctor where FullName like ?", name)

	if dbErr != nil {
		return toReturn, dbErr
	}

	for values.Next() {
		newObject := new(Doctor)

		values.Scan(&newObject.ID, &newObject.FullName, &newObject.Location, &newObject.Specialisation, &newObject.CreatedAt)

		toReturn = append(toReturn, newObject)
	}

	return toReturn, nil
}
