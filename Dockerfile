FROM golang:1.12

WORKDIR $GOPATH/src

COPY . .

RUN go get github.com/gin-gonic/gin
RUN go get github.com/go-sql-driver/mysql
RUN go get github.com/swaggo/gin-swagger
RUN go get github.com/swaggo/gin-swagger/swaggerFiles
RUN go get github.com/alecthomas/template
RUN go get github.com/swaggo/swag

ENV TZ=Australia/Sydney
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN go build -o doctorService

EXPOSE 5000
CMD ["./doctorService"]
