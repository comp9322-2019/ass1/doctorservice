{
    "basePath": "/",
    "info": {
        "contact": {
            "email": "z5061594@student.unsw.edu.au",
            "name": "API Support"
        },
        "description": "Service to create, modify and search doctors",
        "license": {
            "name": "Apache 2.0",
            "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
        },
        "title": "Doctor service",
        "version": "1.0"
    },
    "paths": {
        "/doctor": {
            "get": {
                "consumes": [
                    "application/json"
                ],
                "description": "Get a list of doctors, filterable by name",
                "parameters": [
                    {
                        "description": "Filter by doctors with the given name",
                        "in": "query",
                        "name": "name",
                        "type": "string"
                    }
                ],
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/Doctor"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/ErrorResponse",
                            "type": "object"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/ErrorResponse",
                            "type": "object"
                        }
                    }
                },
                "summary": "Get doctors",
                "tags": [
                    "Doctor"
                ]
            },
            "post": {
                "consumes": [
                    "application/json"
                ],
                "description": "Create a doctor by providing the nessasary doctor information",
                "parameters": [
                    {
                        "description": "Booking information",
                        "in": "body",
                        "name": "BookingInfo",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/DoctorInfo",
                            "type": "object"
                        }
                    }
                ],
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/Doctor",
                            "type": "object"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/ErrorResponse",
                            "type": "object"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/ErrorResponse",
                            "type": "object"
                        }
                    }
                },
                "summary": "Create a Doctor",
                "tags": [
                    "Doctor"
                ]
            }
        },
        "/doctor/{id}": {
            "delete": {
                "consumes": [
                    "application/json"
                ],
                "description": "Deletes a doctor",
                "parameters": [
                    {
                        "description": "Doctor ID to be deleted",
                        "in": "path",
                        "name": "id",
                        "required": true,
                        "type": "string"
                    }
                ],
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/DeleteDoctorResponse",
                            "type": "object"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/ErrorResponse",
                            "type": "object"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/ErrorResponse",
                            "type": "object"
                        }
                    }
                },
                "summary": "Delete a doctor",
                "tags": [
                    "Doctor"
                ]
            },
            "get": {
                "consumes": [
                    "application/json"
                ],
                "description": "Gets a doctor that matches the given ID",
                "parameters": [
                    {
                        "description": "Doctor ID",
                        "in": "path",
                        "name": "id",
                        "required": true,
                        "type": "string"
                    }
                ],
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/Doctor",
                            "type": "object"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/ErrorResponse",
                            "type": "object"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/ErrorResponse",
                            "type": "object"
                        }
                    }
                },
                "summary": "Get a doctor",
                "tags": [
                    "Doctor"
                ]
            },
            "put": {
                "consumes": [
                    "application/json"
                ],
                "description": "Modifies/updates the doctor record at the given ID",
                "parameters": [
                    {
                        "description": "Doctor ID",
                        "in": "path",
                        "name": "id",
                        "required": true,
                        "type": "string"
                    }
                ],
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/Doctor",
                            "type": "object"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/ErrorResponse",
                            "type": "object"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/ErrorResponse",
                            "type": "object"
                        }
                    }
                },
                "summary": "Modify a doctor",
                "tags": [
                    "Doctor"
                ]
            }
        }
    },
    "swagger": "2.0",
    "definitions": {
        "ErrorResponse": {
            "type": "object",
            "properties": {
                "Error": {
                    "type": "string"
                }
            }
        },
        "DoctorInfo": {
            "type": "object",
            "properties": {
                "FullName": {
                    "type": "string"
                },
                "Location": {
                    "type": "string"
                },
                "Specialisation": {
                    "type": "string"
                }
            }
        },
        "DoctorMeta": {
            "type": "object",
            "properties": {
                "ID": {
                    "type": "integer",
                    "format": "int64"
                },
                "CreatedAt": {
                    "type": "integer",
                    "format": "int64"
                }
            }
        },
        "Doctor": {
            "allOf": [
                {
                    "$ref": "#/definitions/DoctorInfo"
                },
                {
                    "$ref": "#/definitions/DoctorMeta"
                }
            ]
        },
        "DeleteDoctorResponse": {
            "type": "object",
            "properties": {
                "DeletedID": {
                    "type": "integer",
                    "format": "int64"
                }
            }
        }
    }
}